FROM node:lts

# install simple http server for serving static content
RUN npm install -g http-server

RUN mkdir /src

WORKDIR /src

COPY package.json /src/package.json
COPY package-lock.json /src/package-lock.json

RUN npm install

COPY . /src

RUN npm run build

EXPOSE 8080

CMD [ "http-server", "dist", "-p", "8080" ]
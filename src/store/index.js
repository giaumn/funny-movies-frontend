import Vue from 'vue'
import Vuex from 'vuex'

import snackbar from './modules/snackbar'
import auth from './modules/auth'
import video from './modules/video'

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    snackbar,
    auth,
    video
  },
  strict: false
})

export default store

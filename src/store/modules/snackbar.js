const state = {
  visibility: false,
  color: null,
  message: null
}

const getters = {
  visibility: (state) => state.visibility,
  color: (state) => state.color,
  message: (state) => state.message
}

const actions = {
  info({commit}, message) {
    commit('SUCCESS', message)
  }
}

const mutations = {
  SET_VISIBILITY: (state, val) => {
    state.visibility = val
  },
  DISMISS: (state) => {
    state.visibility = false
    state.color = null
    state.message = null
  },
  ERROR: (state, message) => {
    state.visibility = true
    state.color = 'error'
    state.message = message
  },
  SUCCESS: (state, message) => {
    state.visibility = true
    state.color = 'success'
    state.message = message
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}

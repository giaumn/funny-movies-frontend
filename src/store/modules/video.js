import VideoService from '../../services/video'
import MyVideoService from '../../services/me/video'

const state = {
  items: []
}

const getters = {
  items: (state) => state.items
}

const actions = {
  index({commit, state}) {
    let params = {
      ts: state.items.length === 0 ? null : state.items[state.items.length - 1].updated_at,
      last_id: state.items.length === 0 ? null : state.items[state.items.length - 1].id
    }
    return VideoService()
      .index(params)
      .then((response) => {
        commit('ADD_ITEMS', response)
      })
  },
  share(store, data) {
    return MyVideoService().create(data)
  },
  react({commit}, data) {
    let params = {
      reaction: {
        reaction_type: data.reactionType
      }
    }
    return VideoService()
      .of(data.video.id)
      .sub('reaction')
      .create(params)
      .then(() => {
        commit('MODIFY_REACTION_COUNT', {video: data.video, reactionType: data.reactionType})
      })
  },
  clear({commit}) {
    return commit('CLEAR_ITEMS')
  }
}

const mutations = {
  CLEAR_ITEMS: (state) => {
    state.items = []
  },
  ADD_ITEMS: (state, response) => {
    response.forEach((item) => state.items.push(item))
  },
  MODIFY_REACTION_COUNT: (state, data) => {
    // Init if not exist
    if (data.video.reactions_by_group[data.reactionType] === undefined) {
      data.video.reactions_by_group = {...data.video.reactions_by_group, [data.reactionType]: 0}
    }

    // Increase 1 for new
    data.video.reactions_by_group[data.reactionType] = data.video.reactions_by_group[data.reactionType] + 1

    // Decrease 1 for old
    const oldReaction = data.video.reaction_of_current_user.reaction_type
    data.video.reactions_by_group[oldReaction] =
      data.video.reactions_by_group[oldReaction] === undefined ? 0 : data.video.reactions_by_group[oldReaction] > 0 ? data.video.reactions_by_group[oldReaction] - 1 : 0

    data.video.reaction_of_current_user.reaction_type = data.reactionType
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}

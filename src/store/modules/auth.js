import TokenService from '../../services/token'
import UserService from '../../services/user'
import MyProfileService from '../../services/me/profile'
import axios from 'axios'

const state = {
  isAuthenticated: false,
  authenticatedUser: {}
}

const getters = {
  hasLocalToken: () => !!localStorage.getItem('auth_token'),
  isAuthenticated: (state) => state.isAuthenticated,
  authToken: () => localStorage.getItem('auth_token'),
  authenticatedUser: (state) => state.authenticatedUser
}

const actions = {
  fetchUser({commit}) {
    return MyProfileService()
      .custom
      .get('')
      .then((response) => {
        commit('SET_AUTHENTICATED_USER', response)
      })
      .catch(() => {
        commit('SET_AUTHENTICATED_USER', {})
      })
  },
  login({commit}, data) {
    return TokenService()
      .create(data)
      .then((response) => {
        response.authToken = `${response.token_type} ${response.token_value}`
        axios.defaults.headers.common['Authorization'] = response.authToken
        commit('LOGIN_SUCCESS', response)
        return MyProfileService()
          .custom
          .get('')
          .then((response) => {
            commit('SET_AUTHENTICATED_USER', response)
          })
      })
  },
  register({dispatch}, data) {
    return UserService()
      .create(data)
      .then(() => {
        let params = {
          provider: 'local',
          email: data.user.email,
          password: data.user.password
        }
        return dispatch('login', params)
      })
  },
  logout({commit}) {
    return commit('CLEAR_CREDENTIALS')
  }
}

const mutations = {
  LOGIN_SUCCESS: (state, data) => {
    state.isAuthenticated = true
    state.authToken = data.authToken
    localStorage.setItem('auth_token', state.authToken)
  },
  SET_AUTHENTICATED_USER: (state, data) => {
    state.isAuthenticated = true
    state.authenticatedUser = data
  },
  CLEAR_CREDENTIALS: (state) => {
    localStorage.removeItem('auth_token')
    state.isAuthenticated = false
    state.authenticatedUser = {}
    state.authToken = null
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}

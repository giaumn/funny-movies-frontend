import Home from './views/Home';
import VideoList from './components/VideoList';
import ShareMovie from './components/ShareMovie';

export default () => [
  {
    path: '',
    component: Home,
    children: [
      {
        path: '',
        name: 'home',
        component: VideoList,
        meta: {
          requireAuth: false
        }
      },
      {
        path: '/share',
        name: 'share',
        component: ShareMovie,
        meta: {
          requireAuth: true
        }
      }
    ]
  }
]
import Vue from 'vue'
import Router from 'vue-router'
import routes from './routes'
import guard from './guard'

Vue.use(Router)

const router = new Router({
  mode: 'history',
  base: '/',
  routes: routes()
})

router.beforeEach(guard)

export default router

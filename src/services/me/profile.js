import Http from '../http'

export default () => new Http('me/profile')

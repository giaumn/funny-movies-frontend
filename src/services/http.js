import axios from 'axios'

function Http(resource, id) {
  this.resource = isNaN(id) ? resource : `${resource}/${id}`

  this.of = (id) => {
    return new Http(this.resource, id)
  }

  this.sub = (subResource) => {
    return new Http(`${this.resource}/${subResource}`)
  }

  this.serializeQuery = (queryData) => {
    let query = []
    for (let p in queryData)
      // eslint-disable-next-line no-prototype-builtins
      if (queryData.hasOwnProperty(p)) {
        if (queryData[p]) {
          query.push(encodeURIComponent(p) + '=' + encodeURIComponent(queryData[p]))
        } else {
          query.push(encodeURIComponent(p) + '=')
        }

      }
    return query.join("&")
  }

  this.axios = axios

  this.index = (params) => {
    const query = this.serializeQuery(params)
    return this.axios.get(`${this.resource}?${query}`)
  }

  this.get = (id, params) => {
    const query = this.serializeQuery(params)
    return this.axios.get(`${this.resource}/${id}?${query}`)
  }

  this.create = (data) => {
    return this.axios.post(this.resource, data)
  }

  this.update = (data) => {
    return this.axios.patch(this.resource, data)
  }

  this.destroy = (id) => {
    return this.axios.delete(`${this.resource}/${id}`)
  }

  this.custom = {
    get: (path) => {
      return this.axios.get(`${this.resource}/${path}`)
    },
    post: (path, data) => {
      return this.axios.post(`${this.resource}/${path}`, data)
    }
  }
}

export default Http

import axios from 'axios'
import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'

let baseUrl = process.env.VUE_APP_API_URL

if (!baseUrl) {
    throw new Error('API URL is not defined')
}

axios.defaults.baseURL = baseUrl
axios.defaults.withCredentials = true
axios.defaults.headers.common['Authorization'] = localStorage.getItem('auth_token') || ''
axios.interceptors.response.use(
  response => response.data,
  error => {
    let message = error.response && error.response.data && error.response.data.error
    message = message ? message : error.message
    store.commit('snackbar/ERROR', message)
    return Promise.reject(error)
  }
)

Vue.config.productionTip = false
Vue.use(Vuex)
Vue.use(VueRouter)

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')

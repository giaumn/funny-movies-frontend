import store from './store';

export default (to, from, next) => {
  const requireAuth = to.matched.some(record => record.meta.requireAuth)
  if (requireAuth) {
    // Check is logged for require auth views
    if (!store.getters['auth/isAuthenticated']) {
      // Not logged in -> to home by default
      next('/')
      return
    }
  }
  next()
}
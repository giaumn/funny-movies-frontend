import '@testing-library/jest-dom'

// browser mocks localStorage
window.localStore = {}
window.localStorageMock = (function() {
  return {
    getItem: function(key) {
      return window.localStore[key] || null
    },
    setItem: function(key, value) {
      window.localStore[key] = value.toString()
    },
    removeItem: function(key) {
      delete window.localStore[key]
    },
    clear: function() {
      window.localStore = {}
    },
  }
})()

Object.defineProperty(window, 'localStorage', {
  value: window.localStorageMock,
})
// browser mocks axios

import Vue from 'vue'
import Vuex from 'vuex'
import Home from '../../../src/views/Home'

// mock <router-view> component
const routerView = {
  name: 'router-view',
  render: h => h('div'),
};

// register mock component
Vue.component('router-view', routerView);

describe('views/Home.vue', () => {
  let store
  let authModule = {}
  let videoModule = {}
  let hasToken = false

  beforeEach(() => {
    authModule = {
      state: {
      },
      getters: {
        hasLocalToken: () => hasToken
      },
      actions: {
        fetchUser: jest.fn()
      },
      namespaced: true
    }

    videoModule = {
      state: {
      },
      getters: {
      },
      actions: {
        clear: jest.fn().mockResolvedValue({}),
        index: jest.fn().mockResolvedValue([])
      },
      namespaced: true
    }

    store = new Vuex.Store({
      modules: {
        auth: authModule,
        video: videoModule
      }
    })
  })

  it('renders home', () => {
    const home = Vue.helpers.shallowMount(Home, {
      store
    })

    const header = home.findComponent({name: 'app-header'})
    const progressBar = home.findComponent({name: 'v-progress-circular'})
    const content = home.findComponent({name: 'v-content'})
    const sheet = content.findComponent({name: 'v-sheet'})
    const routerView = sheet.findComponent({name: 'router-view'})

    expect(home.attributes('fluid')).toBe('true')
    expect(progressBar.exists()).toBe(true)
    expect(progressBar.attributes().class).toBe('progress-circle mt-4 mb-12')
    expect(progressBar.attributes().size).toBe('70')
    expect(progressBar.attributes().width).toBe('7')
    expect(progressBar.attributes().color).toBe('purple')
    expect(progressBar.attributes().indeterminate).toBe('true')
    expect(header.exists()).toBe(true)
    expect(content.exists()).toBe(true)
    expect(sheet.exists()).toBe(true)
    expect(routerView.exists()).toBe(true)
  })

  it('renders home & fetch token', async () => {
    hasToken = true
    Vue.helpers.shallowMount(Home, {
      store
    })
    await Vue.nextTick()
    await Vue.nextTick()
    await Vue.nextTick()
    expect(videoModule.actions.index).toHaveBeenCalledTimes(1)
    expect(authModule.actions.fetchUser).toHaveBeenCalledTimes(1)
  })

  it('renders home & fetch on scroll', async () => {
    hasToken = true
    const home = Vue.helpers.shallowMount(Home, {
      store
    })
    home.vm.fetchScroll = jest.fn()
    window.dispatchEvent(new CustomEvent('scroll', { }))
    expect(home.vm.fetchScroll).toHaveBeenCalledTimes(1)
  })

  it('call fetch on scroll when hit bottom', async () => {
    const home = Vue.helpers.shallowMount(Home, {
      store
    })
    await home.vm.fetchScroll(true, false)
    expect(videoModule.actions.index).toHaveBeenCalledTimes(2)
  })

  it('should not call fetch on scroll when hit bottom if loading', async () => {
    const home = Vue.helpers.shallowMount(Home, {
      store
    })
    await home.vm.fetchScroll(true, true)
    expect(videoModule.actions.index).toHaveBeenCalledTimes(0)
  })

  it('call fetch on back home', async () => {
    const home = Vue.helpers.shallowMount(Home, {
      store
    })
    await home.vm.backToHome()
    expect(videoModule.actions.index).toHaveBeenCalledTimes(2)
  })
})

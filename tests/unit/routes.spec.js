import routes from '../../src/routes'
import Home from '../../src/views/Home'
import VideoList from '../../src/components/VideoList'
import ShareMovie from '../../src/components/ShareMovie'

describe('routes.js', () => {
  it('have proper routes exported', async () => {
    let allRoutes = routes()
    expect(allRoutes).toIncludeSameMembers(
      [
        {
          path: '',
          component: Home,
          children: [
            {
              path: '',
              name: 'home',
              component: VideoList,
              meta: {
                requireAuth: false
              }
            },
            {
              path: '/share',
              name: 'share',
              component: ShareMovie,
              meta: {
                requireAuth: true
              }
            }
          ]
        }
      ]
    )
  })
})

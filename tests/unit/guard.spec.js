import guard from '../../src/guard'
import authModule from '../../src/store/modules/auth'

describe('guard.js', () => {
  it('continue if auth is not required', async () => {
    const next = jest.fn()
    guard({matched: [{meta: {requireAuth: false}}]}, {}, next)

    expect(next).toHaveBeenCalledTimes(1)
    expect(next.mock.calls[0][0]).toBe(undefined)
  })

  it('auth is required, authenticated', async () => {
    authModule.state.isAuthenticated = true
    const next = jest.fn()
    guard({matched: [{meta: {requireAuth: true}}]}, {}, next)

    expect(next).toHaveBeenCalledTimes(1)
    expect(next.mock.calls[0][0]).toBe(undefined)
  })

  it('auth is required, not authenticated', async () => {
    authModule.state.isAuthenticated = false
    const next = jest.fn()
    guard({matched: [{meta: {requireAuth: true}}]}, {}, next)

    expect(next).toHaveBeenCalledTimes(1)
    expect(next.mock.calls[0][0]).toBe('/')
  })
})

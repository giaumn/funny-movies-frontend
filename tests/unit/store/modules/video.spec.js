import axios from 'axios'
import videoModule from '../../../../src/store/modules/video'
import authModule from "../../../../src/store/modules/auth";

describe('store/modules/video.js', () => {
  let state = {}

  describe('getters', () => {

    it('items', () => {
      state.items = [{id: 1, title: 'Video #1'}, {id: 2, title: 'Video #2'}]
      let result = videoModule.getters.items(state)
      expect(result.length).toEqual(2)
      expect(result[0].id).toEqual(1)
      expect(result[0].title).toEqual('Video #1')
      expect(result[1].id).toEqual(2)
      expect(result[1].title).toEqual('Video #2')
    })
  })

  describe('actions', () => {

    beforeEach(() => {
      state.items = []
      axios.get.mockClear()
      axios.post.mockClear()
    })

    it('index', async () => {
      let commit = jest.fn()
      axios.get.mockResolvedValue([{id: 100}]);
      await videoModule.actions.index({commit, state})

      expect(axios.get.mock.calls[0][0]).toEqual('videos?ts=&last_id=')
      expect(commit).toHaveBeenCalledTimes(1)
      expect(commit.mock.calls[0][0]).toEqual('ADD_ITEMS')
      expect(commit.mock.calls[0][1][0].id).toEqual(100)
    })

    it('index error', async () => {
      let commit = jest.fn()
      axios.get.mockRejectedValue({});
      try {
        await videoModule.actions.index({commit, state})
      } catch {
        expect(axios.get.mock.calls[0][0]).toEqual('videos?ts=&last_id=')
        expect(commit).toHaveBeenCalledTimes(0)
      }
    })

    it('index more', async () => {
      let updatedAt = (new Date()).toISOString()
      state.items = [{id: 5, updated_at: updatedAt}, {id: 4, updated_at: updatedAt}]
      let commit = jest.fn()
      axios.get.mockResolvedValue([{id: 3}, {id: 2}, {id: 1}]);
      await videoModule.actions.index({commit, state})

      expect(axios.get.mock.calls[0][0]).toEqual('videos?ts=' + encodeURIComponent(updatedAt) + '&last_id=4')
      expect(commit).toHaveBeenCalledTimes(1)
      expect(commit.mock.calls[0][0]).toEqual('ADD_ITEMS')
      expect(commit.mock.calls[0][1].length).toEqual(3)
      expect(commit.mock.calls[0][1][0].id).toEqual(3)
      expect(commit.mock.calls[0][1][1].id).toEqual(2)
      expect(commit.mock.calls[0][1][2].id).toEqual(1)
    })

    it('share', async () => {
      let commit = jest.fn()
      axios.post.mockResolvedValue({id: 100});
      await videoModule.actions.share({commit, state}, {video_url: 'https://youtube.com/yyy'})

      expect(axios.post.mock.calls[0][0]).toEqual('me/videos')
      expect(commit).toHaveBeenCalledTimes(0)
    })

    it('share error', async () => {
      let commit = jest.fn()
      axios.post.mockRejectedValue({});
      try {
        await videoModule.actions.share({commit, state}, {video_url: 'https://youtube.com/yyy'})
      } catch {
        expect(axios.post.mock.calls[0][0]).toEqual('me/videos')
        expect(commit).toHaveBeenCalledTimes(0)
      }
    })

    it('react', async () => {
      let commit = jest.fn()
      axios.post.mockResolvedValue({id: 100});
      await videoModule.actions.react({commit}, {video: {id: 100}, reactionType: 'like'})

      expect(axios.post.mock.calls[0][0]).toEqual('videos/100/reaction')
      expect(commit).toHaveBeenCalledTimes(1)
      expect(commit.mock.calls[0][0]).toEqual('MODIFY_REACTION_COUNT')
      expect(commit.mock.calls[0][1].video.id).toEqual(100)
      expect(commit.mock.calls[0][1].reactionType).toEqual('like')
    })

    it('clear', async () => {
      let commit = jest.fn()
      await videoModule.actions.clear({commit})

      expect(commit).toHaveBeenCalledTimes(1)
      expect(commit.mock.calls[0][0]).toEqual('CLEAR_ITEMS')
    })
  })

  describe('mutations', () => {
    it('CLEAR_ITEMS', () => {
      state = { items: [{id: 1}]}
      videoModule.mutations.CLEAR_ITEMS(state)
      expect(state.items.length).toEqual(0)
    })

    it('ADD_ITEMS', () => {
      state = { items: [] }
      videoModule.mutations.ADD_ITEMS(state, [{id: 1}, {id: 2}])
      expect(state.items.length).toEqual(2)
      expect(state.items[0].id).toEqual(1)
      expect(state.items[1].id).toEqual(2)
    })

    it('MODIFY_REACTION_COUNT', () => {
      state = { items: [] }
      let video = {id: 1, reactions_by_group: {}, reaction_of_current_user: {}}
      videoModule.mutations.MODIFY_REACTION_COUNT(state, {video: video, reactionType: 'dislike'})
      expect(video.reactions_by_group.dislike).toEqual(1)
    })

    it('MODIFY_REACTION_COUNT increase, decrease', () => {
      state = { items: [] }
      let video = {id: 1, reactions_by_group: {like: 1, dislike: 0}, reaction_of_current_user: {reaction_type: 'like'}}
      videoModule.mutations.MODIFY_REACTION_COUNT(state, {video: video, reactionType: 'dislike'})
      expect(video.reactions_by_group.dislike).toEqual(1)
      expect(video.reactions_by_group.like).toEqual(0)
    })

    it('MODIFY_REACTION_COUNT increase, decrease', () => {
      state = { items: [] }
      let video = {id: 1, reactions_by_group: {like: 2, dislike: 5}, reaction_of_current_user: {reaction_type: 'dislike'}}
      videoModule.mutations.MODIFY_REACTION_COUNT(state, {video: video, reactionType: 'like'})
      expect(video.reactions_by_group.dislike).toEqual(4)
      expect(video.reactions_by_group.like).toEqual(3)
    })
  })
})
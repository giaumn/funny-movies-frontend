import snackbarModule from '../../../../src/store/modules/snackbar'

describe('store/modules/snackbar.js', () => {
  let state = {}

  describe('getters', () => {

    it('visibility', () => {
      state.visibility = true
      let result = snackbarModule.getters.visibility(state)
      expect(result).toEqual(true)
    })

    it('color', () => {
      state.color = 'success'
      let result = snackbarModule.getters.color(state)
      expect(result).toEqual('success')
    })

    it('message', () => {
      state.message = 'message'
      let result = snackbarModule.getters.message(state)
      expect(result).toEqual('message')
    })
  })

  describe('actions', () => {
    it('info', async () => {
      let commit = jest.fn()
      await snackbarModule.actions.info({commit}, 'message')

      expect(commit).toHaveBeenCalledTimes(1)
      expect(commit.mock.calls[0][0]).toEqual('SUCCESS')
      expect(commit.mock.calls[0][1]).toEqual('message')
    })
  })

  describe('mutations', () => {
    it('SET_VISIBILITY', () => {
      state = {}
      snackbarModule.mutations.SET_VISIBILITY(state, true)
      expect(state.visibility).toEqual(true)
    })

    it('DISMISS', () => {
      state = {}
      snackbarModule.mutations.DISMISS(state, true)
      expect(state.visibility).toEqual(false)
      expect(state.color).toEqual(null)
      expect(state.message).toEqual(null)
    })

    it('ERROR', () => {
      state = {}
      snackbarModule.mutations.ERROR(state, 'error message')
      expect(state.visibility).toEqual(true)
      expect(state.color).toEqual('error')
      expect(state.message).toEqual('error message')
    })

    it('SUCCESS', () => {
      state = {}
      snackbarModule.mutations.SUCCESS(state, 'success message')
      expect(state.visibility).toEqual(true)
      expect(state.color).toEqual('success')
      expect(state.message).toEqual('success message')
    })
  })
})
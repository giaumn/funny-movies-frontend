import axios from 'axios'
import authModule from '../../../../src/store/modules/auth'

describe('store/modules/auth.js', () => {
  let state = {}

  describe('getters', () => {

    it('has local token false', () => {
      window.localStore['auth_token'] = null
      let result = authModule.getters.hasLocalToken()
      expect(result).toEqual(false)
    })

    it('has local token true', () => {
      window.localStore['auth_token'] = 'auth_token'
      let result = authModule.getters.hasLocalToken()
      expect(result).toEqual(true)
    })

    it('return isAuthenticated', () => {
      state.isAuthenticated = true
      let result = authModule.getters.isAuthenticated(state)
      expect(result).toEqual(true)
    })

    it('return authToken', () => {
      window.localStore['auth_token'] = 'auth-token'
      state.authToken = 'auth-token'
      let result = authModule.getters.authToken(state)
      expect(result).toEqual('auth-token')
    })

    it('return authenticated user', () => {
      state.authenticatedUser = {id: 1}
      let result = authModule.getters.authenticatedUser(state)
      expect(result.id).toEqual(1)
    })
  })

  describe('actions', () => {

    beforeEach(() => {
      axios.get.mockClear()
      axios.post.mockClear()
    })

    it('fetch user', async () => {
      let commit = jest.fn()
      axios.get.mockResolvedValue({id: 100});
      await authModule.actions.fetchUser({commit})

      expect(axios.get.mock.calls[0][0]).toEqual('me/profile/')
      expect(commit).toHaveBeenCalledTimes(1)
      expect(commit.mock.calls[0][0]).toEqual('SET_AUTHENTICATED_USER')
      expect(commit.mock.calls[0][1].id).toEqual(100)
    })

    it('fetch user error', async () => {
      let commit = jest.fn()
      axios.get.mockRejectedValue({});
      await authModule.actions.fetchUser({commit})

      expect(axios.get.mock.calls[0][0]).toEqual('me/profile/')
      expect(commit).toHaveBeenCalledTimes(1)
      expect(commit.mock.calls[0][0]).toEqual('SET_AUTHENTICATED_USER')
      expect(commit.mock.calls[0][1].id).toEqual(undefined)
    })

    it('login success', async () => {
      let commit = jest.fn()
      axios.get.mockResolvedValue({id: 100});
      axios.post.mockResolvedValue({token_type: 'Bearer', token_value: 'token-value'});
      await authModule.actions.login({commit}, {})

      expect(axios.post.mock.calls[0][0]).toEqual('tokens')
      expect(axios.defaults.headers.common['Authorization']).toEqual('Bearer token-value')
      expect(commit).toHaveBeenCalledTimes(2)
      expect(commit.mock.calls[0][0]).toEqual('LOGIN_SUCCESS')
      expect(commit.mock.calls[0][1].authToken).toEqual('Bearer token-value')
      expect(commit.mock.calls[1][0]).toEqual('SET_AUTHENTICATED_USER')
      expect(commit.mock.calls[1][1].id).toEqual(100)
    })

    it('login error', async () => {
      let commit = jest.fn()
      axios.get.mockRejectedValue({});
      axios.post.mockResolvedValue({token_type: 'Bearer', token_value: 'token-value'});

      try {
        await authModule.actions.login({commit}, {})
      } catch {
        expect(axios.post.mock.calls[0][0]).toEqual('tokens')
        expect(axios.defaults.headers.common['Authorization']).toEqual('Bearer token-value')
        expect(commit).toHaveBeenCalledTimes(1)
        expect(commit.mock.calls[0][0]).toEqual('LOGIN_SUCCESS')
        expect(commit.mock.calls[0][1].authToken).toEqual('Bearer token-value')
      }
    })

    it('register success', async () => {
      let dispatch = jest.fn()
      axios.post.mockResolvedValue({});

      await authModule.actions.register({dispatch}, {
        user: {
          email: 'test@user.com',
          password: '12345678'
        }
      })
      expect(axios.post.mock.calls[0][0]).toEqual('users')
      expect(dispatch).toHaveBeenCalledTimes(1)
      expect(dispatch.mock.calls[0][0]).toEqual('login')
      expect(dispatch.mock.calls[0][1].provider).toEqual('local')
      expect(dispatch.mock.calls[0][1].email).toEqual('test@user.com')
      expect(dispatch.mock.calls[0][1].password).toEqual('12345678')
    })

    it('register error', async () => {
      let dispatch = jest.fn()
      axios.post.mockRejectedValue();

      try {
        await authModule.actions.register({dispatch}, {
          user: {
            email: 'test@user.com',
            password: '12345678'
          }
        })
      } catch {
        expect(axios.post.mock.calls[0][0]).toEqual('users')
        expect(dispatch).toHaveBeenCalledTimes(0)
      }
    })

    it('logout success', async () => {
      let commit = jest.fn()
      await authModule.actions.logout({commit})
      expect(commit).toHaveBeenCalledTimes(1)
      expect(commit.mock.calls[0][0]).toEqual('CLEAR_CREDENTIALS')
    })
  })

  describe('mutations', () => {

    it('LOGIN_SUCCESS', () => {
      window.localStore['auth_token'] = null
      state = {}
      authModule.mutations.LOGIN_SUCCESS(state, {authToken: 'Bearer auth-token'})
      expect(state.isAuthenticated).toEqual(true)
      expect(state.authToken).toEqual('Bearer auth-token')
      expect(window.localStore['auth_token']).toEqual('Bearer auth-token')
    })

    it('SET_AUTHENTICATED_USER', () => {
      window.localStore['auth_token'] = null
      state = {}
      authModule.mutations.SET_AUTHENTICATED_USER(state, {id: 100})
      expect(state.authenticatedUser.id).toEqual(100)
      expect(state.isAuthenticated).toEqual(true)
    })

    it('CLEAR_CREDENTIALS', () => {
      window.localStore['auth_token'] = 'Bearer auth-token'
      state = {}
      authModule.mutations.CLEAR_CREDENTIALS(state, {id: 100})
      expect(state.authenticatedUser.id).toEqual(undefined)
      expect(state.isAuthenticated).toEqual(false)
      expect(state.authToken).toEqual(null)
      expect(window.localStore['auth_token']).toEqual(undefined)
    })
  })
})
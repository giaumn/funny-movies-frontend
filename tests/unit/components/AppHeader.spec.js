import Vue from 'vue'
import Vuex from 'vuex'
import AppHeader from '../../../src/components/AppHeader'

describe('components/AppHeader.vue', () => {
  let store
  let authModule = {}

  beforeEach(() => {
    authModule = {
      state: {
        isAuthenticated: false,
        authenticatedUser: {}
      },
      getters: {
        isAuthenticated: () => authModule.state.isAuthenticated,
        authenticatedUser: () => authModule.state.authenticatedUser
      },
      actions: {
        login: jest.fn(),
        logout: jest.fn(),
        register: jest.fn()
      },
      namespaced: true
    }

    store = new Vuex.Store({
      modules: {
        auth: authModule
      }
    })
  })

  it('renders header', () => {
    const appHeader = Vue.helpers.shallowMount(AppHeader, {
      store
    })

    const dLogin = appHeader.findComponent({name: 'dialog-login'})
    const dRegister = appHeader.findComponent({name: 'dialog-register'})
    const images = appHeader.findAllComponents({name: 'v-img'})
    const buttons = appHeader.findAllComponents({name: 'v-btn'})

    expect(appHeader.exists()).toBe(true)
    expect(appHeader.attributes().app).toBe('true')
    expect(appHeader.attributes().color).toBe('primary')
    expect(appHeader.attributes().dark).toBe('true')
    expect(appHeader.attributes().height).toBe('64')
    expect(appHeader.attributes().fixed).toBe('true')

    expect(dLogin.exists()).toBe(true)
    expect(dRegister.exists()).toBe(true)

    expect(images.length).toBe(2)
    expect(buttons.length).toBe(4)
  })

  it('renders unauthenticated', () => {
    const appHeader = Vue.helpers.mount(AppHeader, {
      store
    })
    const buttons = appHeader.findAllComponents({name: 'v-btn'})
    expect(buttons.length).toBe(4)
    expect(buttons.at(0).text()).toBe('Login')
    expect(buttons.at(1).text()).toBe('Register')
    expect(buttons.at(2).text()).toBe('Login')
    expect(buttons.at(3).text()).toBe('Register')
  })

  it('renders authenticated', () => {
    authModule.state.isAuthenticated = true
    authModule.state.authenticatedUser = { email: 'test@email.com'}
    const appHeader = Vue.helpers.mount(AppHeader, {
      store
    })
    const buttons = appHeader.findAllComponents({name: 'v-btn'})

    expect(buttons.length).toBe(6)
    expect(buttons.at(0).text()).toBe('present_to_all')
    expect(buttons.at(1).text()).toBe('exit_to_app')
    expect(buttons.at(2).text()).toBe('Share a movie')
    expect(buttons.at(3).text()).toBe('Logout')
    expect(buttons.at(4).text()).toBe('Share a movie')
    expect(buttons.at(5).text()).toBe('Logout')
    expect(appHeader.find('span.email').text()).toBe('Welcome, test@email.com')
  })

  it('renders unauthenticated login, register', async () => {
    authModule.state.isAuthenticated = false
    const appHeader = Vue.helpers.mount(AppHeader, {
      store
    })
    const buttons = appHeader.findAllComponents({name: 'v-btn'})
    const images = appHeader.findAllComponents({name: 'v-img'})

    appHeader.vm.$refs.dLogin.show = jest.fn()
    appHeader.vm.$refs.dRegister.show = jest.fn()
    appHeader.vm.$emit = jest.fn()
    appHeader.find('div.email input[type="email"]').setValue('user@test.com')
    appHeader.find('div.password input[type="password"]').setValue('12345678')

    await buttons.at(0).trigger('click')
    await buttons.at(1).trigger('click')
    await buttons.at(2).trigger('click')
    await buttons.at(3).trigger('click')
    await images.at(0).trigger('click')

    expect(appHeader.vm.$refs.dLogin.show).toHaveBeenCalledTimes(1)
    expect(appHeader.vm.$refs.dRegister.show).toHaveBeenCalledTimes(2)
    expect(authModule.actions.login).toHaveBeenCalledTimes(1)
    expect(authModule.actions.login.mock.calls[0][1].provider).toBe('local')
    expect(authModule.actions.login.mock.calls[0][1].email).toBe('user@test.com')
    expect(authModule.actions.login.mock.calls[0][1].password).toBe('12345678')
    expect(buttons.length).toBe(4)
    expect(buttons.at(0).text()).toBe('Login')
    expect(buttons.at(1).text()).toBe('Register')
    expect(buttons.at(2).text()).toBe('Login')
    expect(buttons.at(3).text()).toBe('Register')
    expect(appHeader.find('span.email').exists()).toBe(false)
    expect(appHeader.vm.$emit).toHaveBeenCalledTimes(4)
    expect(appHeader.vm.$emit.mock.calls[2][0]).toBe('login')
    expect(appHeader.vm.$emit.mock.calls[3][0]).toBe('go-home')
  })

  it('renders unauthenticated logout', async () => {
    authModule.state.isAuthenticated = true
    authModule.actions.logout.mockResolvedValue({})
    const appHeader = Vue.helpers.mount(AppHeader, {
      store
    })
    const buttons = appHeader.findAllComponents({name: 'v-btn'})

    appHeader.vm.$emit = jest.fn()
    appHeader.vm.$refs.dLogin.show = jest.fn()

    await buttons.at(3).trigger('click')
    await buttons.at(5).trigger('click')

    expect(authModule.actions.logout).toHaveBeenCalledTimes(2)
    expect(appHeader.vm.$emit).toHaveBeenCalledTimes(2)
    expect(appHeader.vm.$emit.mock.calls[1][0]).toBe('logout')
  })
})

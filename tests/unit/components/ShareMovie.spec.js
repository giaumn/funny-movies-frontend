import Vue from 'vue'
import Vuex from 'vuex'
import ShareMovie from '../../../src/components/ShareMovie'

describe('components/ShareMovie.vue', () => {
  let store
  let videoModule

  beforeEach(() => {
    videoModule = {
      state: {
      },
      getters: {
      },
      actions: {
        share: jest.fn()
      },
      namespaced: true
    }

    store = new Vuex.Store({
      modules: {
        video: videoModule
      }
    })
  })

  it('renders dialog register', async () => {
    const wrapper = Vue.helpers.mount(ShareMovie, {
      store
    })

    const card = wrapper.findComponent({name: 'v-card'})
    const title = wrapper.find('.title')
    const subtitle = wrapper.find('.subtitle')
    const button = wrapper.findComponent({name: 'v-btn'})

    wrapper.vm.$store.commit = jest.fn()
    videoModule.actions.share.mockResolvedValue({})
    wrapper.find('div.video-url input[type="text"]').setValue('https://youtube.com/abc')

    await button.trigger('click')
    await Vue.nextTick()

    expect(card.exists()).toBe(true)
    expect(title.exists()).toBe(true)
    expect(subtitle.exists()).toBe(true)
    expect(button.exists()).toBe(true)
    expect(title.text()).toBe('Share a Youtube movie')
    expect(subtitle.text()).toBe('Enter a Youtube link and hit below button to share your movie.')
    expect(button.text()).toBe('SHARE')
    expect(videoModule.actions.share).toHaveBeenCalledTimes(1)
    expect(videoModule.actions.share.mock.calls[0][1].video.video_url).toBe('https://youtube.com/abc')
    expect(wrapper.vm.$store.commit).toHaveBeenCalledTimes(1)
    expect(wrapper.vm.$store.commit.mock.calls[0][0]).toBe('snackbar/SUCCESS')
    expect(wrapper.vm.$store.commit.mock.calls[0][1]).toBe('Congrats! Your movie has been successfully shared.')
  })

})

import Vue from 'vue'
import Vuex from 'vuex'
import VideoItem from '../../../src/components/VideoItem'

describe('components/VideoItem.vue', () => {
  let store
  let authModule
  let videoModule

  beforeEach(() => {
    authModule = {
      state: {
        isAuthenticated: false
      },
      getters: {
        isAuthenticated: () => authModule.state.isAuthenticated
      },
      actions: {
      },
      namespaced: true
    }

    videoModule = {
      state: {
      },
      getters: {
      },
      actions: {
        react: jest.fn()
      },
      namespaced: true
    }

    store = new Vuex.Store({
      modules: {
        auth: authModule,
        video: videoModule
      }
    })
  })

  it('renders video item unauthenticated user', async () => {
    const wrapper = Vue.helpers.mount(VideoItem, {
      store,
      propsData: {
        video: {
          embed_url: 'xxx',
          title: 'Video #1',
          user: {
            email: 'test@user.com'
          },
          reactions_by_group: {
            like: 10,
            dislike: 17
          },
          description: 'Video #1 description',
          reaction_of_current_user: {
            reaction_type: 'like'
          }
        }
      }
    })

    const iframe = wrapper.find('iframe')
    const title = wrapper.find('.title')
    const sharedBy = wrapper.find('.user-shared')
    const like = wrapper.find('.like-reaction')
    const dislike = wrapper.find('.dislike-reaction')
    const descTitle = wrapper.find('.desc')
    const description = wrapper.find('.desc-value')
    const buttons = wrapper.findAllComponents({ name: 'v-btn' })

    expect(iframe.exists()).toBe(true)
    expect(title.exists()).toBe(true)
    expect(sharedBy.exists()).toBe(true)
    expect(like.exists()).toBe(true)
    expect(dislike.exists()).toBe(true)
    expect(descTitle.exists()).toBe(true)
    expect(description.exists()).toBe(true)

    expect(iframe.attributes('src')).toBe('xxx')
    expect(title.text()).toBe('Video #1')
    expect(sharedBy.text()).toBe('Shared by: test@user.com')
    expect(like.text()).toBe('10')
    expect(dislike.text()).toBe('17')
    expect(descTitle.text()).toBe('Description:')
    expect(description.text()).toBe('Video #1 description')
    expect(buttons.length).toBe(0)
  })

  it('renders video item authenticated user', async () => {
    authModule.state.isAuthenticated = true
    const wrapper = Vue.helpers.mount(VideoItem, {
      store,
      propsData: {
        video: {
          embed_url: 'xxx',
          title: 'Video #1',
          user: {
            email: 'test@user.com'
          },
          reactions_by_group: {
            like: 10,
            dislike: 17
          },
          description: 'Video #1 description',
          reaction_of_current_user: {
          }
        }
      }
    })

    const buttons = wrapper.findAllComponents({ name: 'v-btn' })

    expect(buttons.length).toBe(2)
  })

  it('renders video item authenticated user liked', async () => {
    authModule.state.isAuthenticated = true
    const wrapper = Vue.helpers.mount(VideoItem, {
      store,
      propsData: {
        video: {
          embed_url: 'xxx',
          title: 'Video #1',
          user: {
            email: 'test@user.com'
          },
          reactions_by_group: {
            like: 10,
            dislike: 17
          },
          description: 'Video #1 description',
          reaction_of_current_user: {
            reaction_type: 'like'
          }
        }
      }
    })

    const button = wrapper.findComponent({ name: 'v-btn' })

    await button.trigger('click')

    expect(button.text()).toBe('thumb_down')
    expect(videoModule.actions.react).toHaveBeenCalledTimes(1)
    expect(videoModule.actions.react.mock.calls[0][1].video.embed_url).toBe('xxx')
    expect(videoModule.actions.react.mock.calls[0][1].reactionType).toBe('dislike')
  })

  it('renders video item authenticated user disliked', async () => {
    authModule.state.isAuthenticated = true
    const wrapper = Vue.helpers.mount(VideoItem, {
      store,
      propsData: {
        video: {
          embed_url: 'xxx',
          title: 'Video #1',
          user: {
            email: 'test@user.com'
          },
          reactions_by_group: {
            like: 10,
            dislike: 17
          },
          description: 'Video #1 description',
          reaction_of_current_user: {
            reaction_type: 'dislike'
          }
        }
      }
    })

    const button = wrapper.findComponent({ name: 'v-btn' })

    await button.trigger('click')

    expect(button.text()).toBe('thumb_up')
    expect(videoModule.actions.react).toHaveBeenCalledTimes(1)
    expect(videoModule.actions.react.mock.calls[0][1].video.embed_url).toBe('xxx')
    expect(videoModule.actions.react.mock.calls[0][1].reactionType).toBe('like')
  })
})

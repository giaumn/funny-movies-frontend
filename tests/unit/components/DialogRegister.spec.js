import Vue from 'vue'
import Vuex from 'vuex'
import DialogRegister from '../../../src/components/DialogRegister'

describe('components/DialogRegister.vue', () => {
  let store
  let authModule = {}

  beforeEach(() => {
    authModule = {
      state: {
      },
      getters: {
      },
      actions: {
        register: jest.fn()
      },
      namespaced: true
    }

    store = new Vuex.Store({
      modules: {
        auth: authModule
      }
    })
  })

  it('renders dialog register', async () => {
    document.body.setAttribute('data-app', true) // fix vuetify warning
    const wrapper = Vue.helpers.mount(DialogRegister, {
      store
    })
    await wrapper.setData({ visibility: true })
    const dialog = wrapper.findComponent({name: 'v-dialog'})
    const card = wrapper.findComponent({name: 'v-card'})
    const title = wrapper.find('.headline')
    const subtitle = wrapper.find('.subtitle')
    const button = wrapper.findComponent({name: 'v-btn'})

    authModule.actions.register.mockResolvedValue({})
    wrapper.find('div.email input[type="email"]').setValue('user@test.com')
    wrapper.find('div.password input[type="password"]').setValue('12345678')
    wrapper.find('div.password-confirmation input[type="password"]').setValue('12345678')

    await button.trigger('click')
    await Vue.nextTick()

    expect(dialog.exists()).toBe(true)
    expect(card.exists()).toBe(true)
    expect(title.exists()).toBe(true)
    expect(subtitle.exists()).toBe(true)
    expect(button.exists()).toBe(true)
    expect(title.text()).toBe('Register')
    expect(subtitle.text()).toBe('Please input your data to sign up')
    expect(button.text()).toBe('Register')
    expect(authModule.actions.register).toHaveBeenCalledTimes(1)
    expect(authModule.actions.register.mock.calls[0][1].user.email).toBe('user@test.com')
    expect(authModule.actions.register.mock.calls[0][1].user.password).toBe('12345678')
    expect(authModule.actions.register.mock.calls[0][1].user.password_confirmation).toBe('12345678')
    expect(wrapper.vm.visibility).toBe(false)

    await wrapper.vm.show()
    expect(wrapper.vm.visibility).toBe(true)
  })

})

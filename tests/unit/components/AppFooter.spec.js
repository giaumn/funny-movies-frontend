import Vue from 'vue'
import Vuex from 'vuex'
import AppFooter from '../../../src/components/AppFooter'

describe('components/AppFooter.vue', () => {
  let store
  let authModule = {}

  beforeEach(() => {
    authModule = {
      state: {
        isAuthenticated: false,
        authenticatedUser: {}
      },
      getters: {
        isAuthenticated: () => authModule.state.isAuthenticated,
        authenticatedUser: () => authModule.state.authenticatedUser
      },
      actions: {
        login: jest.fn(),
        logout: jest.fn(),
        register: jest.fn()
      },
      namespaced: true
    }

    store = new Vuex.Store({
      modules: {
        auth: authModule
      }
    })
  })

  it('renders footer', () => {
    const appFooter = Vue.helpers.shallowMount(AppFooter, {
      store
    })

    expect(appFooter.exists()).toBe(true)
    expect(appFooter.attributes().app).toBe('true')
    expect(appFooter.attributes().dark).toBe('true')
    expect(appFooter.attributes().padless).toBe('true')
    expect(appFooter.text()).toBe('Funny Movies - Copyright © 2020')
  })
})

import Vue from 'vue'
import Vuex from 'vuex'
import DialogLogin from '../../../src/components/DialogLogin'

describe('components/DialogLogin.vue', () => {
  let store
  let authModule = {}

  beforeEach(() => {
    authModule = {
      state: {
      },
      getters: {
      },
      actions: {
        login: jest.fn()
      },
      namespaced: true
    }

    store = new Vuex.Store({
      modules: {
        auth: authModule
      }
    })
  })

  it('renders dialog login', async () => {
    document.body.setAttribute('data-app', true) // fix vuetify warning
    const wrapper = Vue.helpers.mount(DialogLogin, {
      store
    })
    await wrapper.setData({ visibility: true })
    const dialog = wrapper.findComponent({name: 'v-dialog'})
    const card = wrapper.findComponent({name: 'v-card'})
    const title = wrapper.find('.headline')
    const subtitle = wrapper.find('.subtitle')
    const button = wrapper.findComponent({name: 'v-btn'})

    authModule.actions.login.mockResolvedValue({})
    wrapper.find('div.email input[type="email"]').setValue('user@test.com')
    wrapper.find('div.password input[type="password"]').setValue('12345678')

    await button.trigger('click')
    await Vue.nextTick()

    expect(dialog.exists()).toBe(true)
    expect(card.exists()).toBe(true)
    expect(title.exists()).toBe(true)
    expect(subtitle.exists()).toBe(true)
    expect(button.exists()).toBe(true)
    expect(title.text()).toBe('Login')
    expect(subtitle.text()).toBe('Please input your email and password to login')
    expect(button.text()).toBe('Login')
    expect(authModule.actions.login).toHaveBeenCalledTimes(1)
    expect(authModule.actions.login.mock.calls[0][1].provider).toBe('local')
    expect(authModule.actions.login.mock.calls[0][1].email).toBe('user@test.com')
    expect(authModule.actions.login.mock.calls[0][1].password).toBe('12345678')
    expect(wrapper.vm.visibility).toBe(false)

    await wrapper.vm.show()
    expect(wrapper.vm.visibility).toBe(true)
  })

})

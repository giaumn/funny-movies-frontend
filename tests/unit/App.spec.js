import Vue from 'vue'
import Vuex from 'vuex'
import App from '../../src/App'
import snackbarModule from '../../src/store/modules/snackbar'

// mock <router-view> component
const routerView = {
  name: 'router-view',
  render: h => h('div'),
};

// register mock component
Vue.component('router-view', routerView);

describe('App.vue', () => {
  let state
  let store
  let mutations

  beforeEach(() => {
    state = {
      visibility: false,
      color: 'success'
    }

    mutations = {
      DISMISS: (state) => { state.visibility = false},
      SET_VISIBILITY: (state, visibility) => { state.visibility = visibility }
    }

    store = new Vuex.Store({
      modules: {
        snackbar: {
          state,
          getters: snackbarModule.getters,
          mutations,
          namespaced: true
        }
      }
    })
  })

  it('renders v-app and v-snackbar', () => {
    const app = Vue.helpers.shallowMount(App, {
      store
    })
    const routerView = app.findComponent({name: 'router-view'})
    const snackbar = app.findComponent({name: 'v-snackbar'})

    expect(app.attributes('id')).toBe('app')
    expect(routerView.exists()).toBe(true)
    expect(snackbar.exists()).toBe(true)
    expect(snackbar.find('.v-snack').exists()).toBe(false)
    expect(snackbar.attributes().timeout).toBe('7000')
    expect(snackbar.attributes().color).toBe('success')
    expect(snackbar.attributes().top).toBe('true')
    expect(snackbar.attributes().right).toBe('true')
    expect(snackbar.attributes().absolute).toBe('true')
  })

  it('renders v-app and hide v-snackbar', () => {
    const app = Vue.helpers.mount(App, {
      store
    })
    const snackbar = app.findComponent({name: 'v-snackbar'})

    expect(app.attributes('id')).toBe('app')
    expect(snackbar.exists()).toBe(true)
    expect(snackbar.find('.v-snack').exists()).toBe(false)
  })

  it('renders v-app and show v-snackbar', async () => {
    state.visibility = true
    const app = Vue.helpers.mount(App, {
      store
    })
    const snackbar = app.findComponent({name: 'v-snackbar'})
    const button = snackbar.findComponent({name: 'v-btn'})
    expect(snackbar.find('.v-snack').exists()).toBe(true)
    expect(button.exists()).toBe(true)
    await button.trigger('click')
    expect(snackbar.find('.v-snack').exists()).toBe(false)
  })

  it('renders v-app and v-snackbar disappear', async () => {
    state.visibility = true
    const app = Vue.helpers.mount(App, {
      store
    })
    const snackbar = app.findComponent({name: 'v-snackbar'})
    const button = snackbar.findComponent({name: 'v-btn'})
    expect(snackbar.find('.v-snack').exists()).toBe(true)
    expect(button.exists()).toBe(true)
    await app.setData({ visible: false })
    expect(snackbar.find('.v-snack').exists()).toBe(false)
  })
})

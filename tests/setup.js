// test/setup.js

import Vue from 'vue'
import Vuetify from 'vuetify'
import Vuex from 'vuex';
import VueRouter from 'vue-router'
import { createLocalVue, mount, shallowMount } from '@vue/test-utils';
import router from '../src/router'
import axios from 'axios'

const localVue = createLocalVue()

const vuetify = new Vuetify()

const testPlugin = {
    install () {
        Vue.helpers = helpers
        Vue.prototype.$helpers = helpers
    }
}

const helpers = {
    shallowMount: (component, options) => {
        return shallowMount(component, {
            localVue,
            vuetify,
            router,
            ...options,
        })
    },
    mount: (component, options) => {
        return mount(component, {
            localVue,
            vuetify,
            router,
            ...options,
        })
    }
}

Vue.use(testPlugin)
Vue.use(Vuetify)
localVue.use(VueRouter)
localVue.use(Vuex)

// Disable transition for testing
const div = {
    functional: true,
    render: (h, { data, children }) => h('div', data, children),
}

Vue.component('transition', div)
Vue.component('transition-group', div)

axios.get = jest.fn()
axios.post = jest.fn()
axios.put = jest.fn()
axios.patch = jest.fn()
axios.delete = jest.fn()